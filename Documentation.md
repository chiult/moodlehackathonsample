# Documentation

### Incidents and Lessons Learned

##### Design Process
- We took a long time to figure out what exactly are the requirements. The instructor’s inputs are helpful in navigating through this obstacle. 
- We had a hard time wrapping our heads around how everything comes together. For a while we thought getting Docker up and running was the only priority. Thus we lost some time there. 
- Noting down these requirements and our process of approaching the issues is crucial in improving our service quality.  If there are similar projects in the future, the experience in this project could be an inspiration. 

##### Docker
- We tried using existing moodle images, however, it didn’t work out. Then we started everything all over, writing our own Dockerfile to generate Moodle images.
- Docker installed on VM did not produce desired output. Therefore local machines are used to demonstrate how Docker can be used for deployment and testing.
- Quick prototyping of docker containers on VMs would save a lot of time and energy. 

##### Jenkins
- On our VM the Docker was not working. None of us have Jenkins on our laptop and we were running out of time. Therefore, in our presentation we only presented the logic and how Jenkins plays a role in our project.
- Since Jenkins and Docker are so closely related to one another, there is not much to be done on Jenkins end if Docker doesn’t work. However, thinking about what to put in Jenkinsfile(e.g putting Dockerfile flag) could save us time in the long run. 

##### ElasticSearch
- We had difficulty finding the tags for our team. This obstacle was significant because without the tags, there would be no way of filtering out useful data for our team. This is fixed by changing up a bit of configurations in Filebeat.yml. 
- We also did not realize we were using templates on elasticsearch. When filebeat is run, we needed to choose the Filebeat-* option and filter data using our team tags. The rest of the work is finding out what are some useful visualizations we can put on the dashboard. We decided to include metricbeat, filebeat as well as logs that are specific to our team.
- If the public domain for ElasticSearch does not work, backup for private servers must be prepared to handle the incident. 

##### AppDynamics
- We were unable to connect to the application server due to licensing issues. 
- Due to this issue, we were incapable of testing whether the alerting system developed would work or not
- The hurdles such licensing issues must be cleared before actual monitoring is started in order to maintain efficiency. 

##### ITRS
- The rules we used to check for the severity of our metrics were not running. This was mostly due to how the targets were set up and how we named specific metrics. This issue was solved by broadening the path used to target the metric and renaming our them so that no conflict would occur with any other metric with similar name in our gateway
- While setting up a rule and an action to automatically restart both our app and database processes in case if they went down, we were not able to create the script needed for the action to take place. This was mostly due to time constraints we had for the project, and also our low expertise when it comes to understanding how to utilize certain features that ITRS provides.
- If there are multiple teams working on configurations at the same time, different include groups must be created to avoid conflicts and errors. 
