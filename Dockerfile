FROM php:7.2-apache

#install addtional software
RUN apt-get update && apt-get install -y \
    git \
    libzip-dev libpng-dev libicu-dev libxml2-dev zip \
&& rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-configure zip --with-libzip && \
    docker-php-ext-install mysqli zip gd intl xmlrpc soap \
    && docker-php-ext-enable opcache

RUN apt-get purge

#git clone moodle
#RUN git clone https://github.com/moodle/moodle.git 

# copy all moodle file to 
COPY moodle/ /var/www/html/

RUN mkdir /var/moodledata && chown -R www-data /var/moodledata \
	&& chmod -R 777 /var/moodledata \
	&& chmod -R 0755 /var/www/html/

#copy all moodledata file
#COPY moodledata/ /var/moodledata
